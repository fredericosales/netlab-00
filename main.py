#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
import pdb
import paho.mqtt.client as mqtt
import logging
import datetime
import time
import warnings


def warn(*args, **kwargs):
    pass


warnings.warn = warn


# =========================================================================== #
# Class

class Dummy:
    """Dummy class."""

    def __init__(self, broker="localhost", port=1883, keepalive=60, transport="tcp", cli_name="mito", topic="PGCC/Lights/NetLab", payload="off", qos=0):
        """Constructor."""

        self.broker = broker
        self.port = port
        self.keepalive = keepalive
        self.transport = transport
        self.cli_name = cli_name
        self.topic = topic
        self.payload = payload
        self.qos = qos

    def log_file(self, name):
        """Create a logfile."""

        tstamp = datetime.date.today()
        format = ''
        logfilename = name + '-' + str(tstamp) + '.log'
        logging.basicConfig(filename=logfilename, level=logging.INFO, format=format)

    def ttime(self):
        """Mark time."""

        ttime = time.time()
        return ttime

    def send_mqtt(self):
        """
        Its a kind of magic.
        Send a dummy message to Mosquitto broker over TCP or Websockets
        """

        # set unique name
        client = mqtt.Client(self.cli_name, self.transport)

        # set a connection to Mosquitto broker
        if self.transport == "tcp":
            client.connect(self.broker, self.port, self.keepalive)
        else:
            client.connect(self.broker, self.port)

        # subscribe to broker
        client.subscribe(self.topic)

        # publish
        client.publish(self.topic, self.payload, self.qos)


# =========================================================================== #
# main

if __name__ == '__main__':
    # do some

    # Edelberto (mosquitto ver conf)
    # user = Dummy(payload="off", broker="200.131.219.102")
    # user.log_file("200.131.219.102_tcp")
    user = Dummy(payload="off", broker="200.131.219.102", transport="websockets")
    user.log_file("200.131.219.102_websockets")

    # corexy (mosquitto raspbian 9 Stretch zero w 1Ghz 512Mb)
    # user = Dummy(payload="off", broker="corexy.local")
    # user.log_file("corexy.local_tcp")
    # user = Dummy(payload="off", broker="corexy.local", transport="websockets")
    # user.log_file("corexy.local_websockets")

    # netlab-00 (mosquitto raspbian 9 Stretch 700Mhz 512Mb)
    # user = Dummy(payload="off", broker="netlab-00.local")
    # user.log_file("netlab-00.local_tcp")
    # user = Dummy(payload="off", broker="netlab-00.local", transport="websockets)
    # user.log_file("netlab-00.local_websockets")

    # time count
    begin = user.ttime()

    # sending msgs
    for i in range(1, 10001):
        if i % 2 == 0:
            _on = user.ttime()
            logging.info("{},{:.2f}".format(i, (_on - begin)))
            user.payload = "on"
            user.send_mqtt()
        else:
            _off = user.ttime()
            logging.info("{},{:.2f}".format(i, (_off - begin)))
            user.payload = "off"
            user.send_mqtt()
